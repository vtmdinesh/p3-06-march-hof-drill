const cardDetails = require("../p3.js")
const fs = require("fs")

const addCVVtoCards = require("../chainingHOF.js")

let result = addCVVtoCards(cardDetails)

fs.writeFile("../output/sortedValidCards.json", JSON.stringify(result), (err) => {
    if (err) {
        console.error(err)
    }
    else {
        console.info("File Written Successfully")
    }
})

console.log(result)