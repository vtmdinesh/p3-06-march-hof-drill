const cardsIssuedBeforeJune = (cardDetails) => {
    let filteredCards = cardDetails.filter((eachData) => {
        let issuedDate = eachData.issue_date;
        let date = new Date(issuedDate);
        let monthOfIssueDate = date.getMonth();
        if (monthOfIssueDate < 5) {
            return eachData;
        }
    });
    return filteredCards;
}

module.exports = cardsIssuedBeforeJune