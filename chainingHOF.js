const addCVVtoCards = (cardDetails) => {

    const sortingObject = (obj, monthsArray) => {
        newObj = {}
        monthsArray.map(key => {
            if (!obj[key] === undefined) {
                newObj[key] = []
            }
            else {
                newObj[key] = obj[key]
            }
        });
        return newObj

    }

    let monthsArray = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]

    let monthObject = cardDetails.map(eachCard => {

        const CVV = Math.floor(Math.random() * 999 + 100);
        eachCard.CVV = CVV;

        let issuedDate = eachCard.issue_date;
        let date = new Date(issuedDate);
        let monthOfIssue = date.getMonth();

        if (monthOfIssue < 2) {
            eachCard.status = "Invalid"
        } else {
            eachCard.status = "Valid"
        }

        return eachCard
    })
        .sort((a, b) => (new Date(a.issue_date) < new Date(b.issue_date)) ? -1 : 1)
        .reduce((monthObject, card) => {

            let monthIndex = new Date(card.issue_date).getMonth()

            let month = monthsArray[monthIndex]

            if (!monthObject[month]) {
                monthObject[month] = []
            }
            monthObject[month].push(card)

            return monthObject
        }, {})

    return sortingObject(monthObject, monthsArray)


}

module.exports = addCVVtoCards