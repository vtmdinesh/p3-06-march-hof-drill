

const oddnumbersInEvenPositions = (cardDetails) => {
    let result = (cardDetails).filter((cardDetail) => {
        let cardNumberArray = cardDetail.card_number.split("")
        let sum = 0
        cardNumberArray.map((number, index) => {
            if (index % 2 === 0) {
                sum += parseInt(number)
            }
        })

        return (sum % 2 !== 0) ? true : false


    })

    return result
}

module.exports = oddnumbersInEvenPositions